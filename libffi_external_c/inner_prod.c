#include "inner_prod.h"

int inner_prod(int * x, int n)
{
  int res = 0;
  for (int i = 0; i < n; i++) {
    res += x[i]*x[i];
  }
  return res;
}

int inner_prod2(Arr * a)
{
  int res = 0;
  Arr b = *a;
  for (int i = 0; i < b.n; i++) {
    res += b.x[i]*b.x[i];
  }
  return res;
}
