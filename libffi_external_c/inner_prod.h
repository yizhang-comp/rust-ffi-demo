#ifndef TEST_H
#define TEST_H

typedef struct {
  int * x;
  int n;
} Arr;

int inner_prod(int *x, int n);
int inner_prod2(Arr * a);

#endif
