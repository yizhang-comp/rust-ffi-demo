#include <stdio.h>
#include <stdlib.h>
#include "inner_prod.h"

void main() {
  int n = 5;
  int a[5] = {1, 2, 3, 4, 5};
  int ip = inner_prod(a, n);
  printf("%d\n", ip);

  Arr ar;
  ar.x = a;
  ar.n = 5;
  ip = inner_prod2(&ar);
  printf("%d\n", ip);
}
