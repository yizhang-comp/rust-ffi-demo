//! When calling a C function that takes pointer to an array
//! as argument in Rust, we can pass the reference of the
//! first element of the Rust array as raw pointer.

extern crate libc;

#[allow(unused_imports)]
use libc::{c_int, int8_t, uint32_t, int64_t, c_char, c_double, c_float};

#[derive(Debug, Clone)]
#[repr(C)]
pub struct Arr {
    pub x: *mut c_int,
    pub n: c_int,
}

#[link(name = "ffi_external_c")]
extern "C" {
    pub fn inner_prod(x: *mut c_int, n: c_int) -> c_int;
    pub fn inner_prod2(a: *mut Arr) -> c_int;
}

fn main() {
    let mut x: [c_int; 5] = [1, 2, 3, 4, 5];
    let n: c_int = 5;
    let mut res: c_int = unsafe { inner_prod(&mut x[0] as *mut c_int, n) };
    println!("Inner product of {:?} is {:?}", x, res);

    let mut a: Arr = Arr {
        x: &mut x[0] as *mut c_int,
        n: n,
    };
    res = unsafe { inner_prod2(&mut a as *mut Arr) };
    println!("Inner product of {:?} is {:?}", a.x, res);

}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {}
}
